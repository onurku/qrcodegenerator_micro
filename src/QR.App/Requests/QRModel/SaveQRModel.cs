using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using QR.App.Data;
using QR.App.Requests.Model;
using QRCoder;

namespace QR.App.Requests.QRModel
{
  public class SaveQRModel
  {
    public class Request : IRequest<ResultDto<QRModelDto>>
    {
      public int Id { get; set; }
      public string UserId { get; set; }
      public QRModelPostDto Post { get; set; }
    }

    public class Handler : IRequestHandler<Request, ResultDto<QRModelDto>>
    {
      private readonly ApplicationDbContext _context;
      private readonly IMapper _mapper;

      public Handler(ApplicationDbContext context, IMapper mapper)
      {
        _context = context ?? throw new ArgumentNullException(nameof(context));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
      }

      public async Task<ResultDto<QRModelDto>> Handle(Request request, CancellationToken cancellationToken)
      {
        var result = new ResultDto<QRModelDto>();
        if (request.Post == null) throw new ArgumentNullException(nameof(request.Post));

        var entity = _mapper.Map<Data.Model.QRModel>(request.Post);
        var user = await _context.Users.FindAsync(request.UserId);
        entity.UpdatedBy = user;
        entity.UpdateDate = DateTime.Now;
        if (request.Id <= default(int))
        {
          entity.CreatedBy = user;
          entity.CreateDate = DateTime.Now;

          Bitmap qrCodeImage = GenerateQRCode(entity.Url, entity.Width, entity.LogoExist ? "Angular.png" : null, entity.ColorDark, entity.ColorLight, entity.Title);
          // QR Code resmini byte[] olarak dönüştürün
          byte[] qrCodeImageData;
          using (MemoryStream ms = new MemoryStream())
          {
            qrCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            qrCodeImageData = ms.ToArray();
          }

          entity.QRCodeImage = qrCodeImageData;
          _context.QRModel.Add(entity);

        }
        else
        {
          entity.Id = request.Id;
          _context.QRModel.Update(entity);
        }

        await _context.SaveChangesAsync(cancellationToken);
        result.Result = _mapper.Map<QRModelDto>(entity);

        return result;
      }

      public static Bitmap GenerateQRCode(string url, int height, string logoFilePath = null, string darkColor = null, string lightColor = null, string title = null)
      {
        // QR Code Generate
        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        QRCodeData qrCodeData = qrGenerator.CreateQrCode(url, QRCodeGenerator.ECCLevel.Q);
        QRCode qrCode = new QRCode(qrCodeData);



        // QR kodunu bir bitmap olarak oluştur

        Bitmap qrCodeImage = qrCode.GetGraphic(20, darkColor, lightColor);

        // Logo place
        if (!string.IsNullOrEmpty(logoFilePath))
        {
          string fullPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", logoFilePath);
          Bitmap logo = new Bitmap(fullPath);

          // Logo boyutunu değiştir
          int logoWidth = (int)(qrCodeImage.Width * 0.2); // %20 genişlet
          int logoHeight = (int)(qrCodeImage.Height * 0.2); // %20 yükseklet
          Image resizedLogo = logo.GetThumbnailImage(logoWidth, logoHeight, null, IntPtr.Zero);

          // Logoyu resmin merkezine yerleştirin
          int centerX = (qrCodeImage.Width - resizedLogo.Width) / 2;
          int centerY = (qrCodeImage.Height - resizedLogo.Height) / 2;
          Rectangle logoRect = new Rectangle(centerX, centerY, resizedLogo.Width, resizedLogo.Height);

          // Logoyu resme çizin
          using (Graphics graphics = Graphics.FromImage(qrCodeImage))
          {
            graphics.DrawImage(resizedLogo, logoRect);
          }

          // Title
          if (!string.IsNullOrEmpty(title))
          {
            int titleHeight = 50;
            int titleWidth = qrCodeImage.Width;
            Bitmap titleImage = new Bitmap(titleWidth, titleHeight);
            using (Graphics graphics = Graphics.FromImage(titleImage))
            {
              graphics.Clear(Color.White);
              StringFormat stringFormat = new StringFormat();
              stringFormat.Alignment = StringAlignment.Center;
              stringFormat.LineAlignment = StringAlignment.Center;
              graphics.DrawString(title, new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new RectangleF(0, 0, titleWidth, titleHeight), stringFormat);
            }
            Bitmap mergedImage = new Bitmap(qrCodeImage.Width, qrCodeImage.Height + titleHeight);
            using (Graphics graphics = Graphics.FromImage(mergedImage))
            {
              graphics.Clear(Color.White);
              graphics.DrawImage(qrCodeImage, 0, 0);
              graphics.DrawImage(titleImage, 0, qrCodeImage.Height);
            }
            return mergedImage;
          }
          else
          {
            return qrCodeImage;
          }

        }

        return qrCodeImage;
      }

    }
  }
}