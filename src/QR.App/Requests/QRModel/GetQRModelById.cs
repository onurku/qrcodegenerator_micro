using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.QRModel
{
    public class GetQRModelById
    {
        public class Request : IRequest<ResultDto<QRModelDto>>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Request, ResultDto<QRModelDto>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }

            public async Task<ResultDto<QRModelDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<QRModelDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity =
                    await _context.QRModel.FirstOrDefaultAsync(x => x.Id == request.Id,
                        cancellationToken: cancellationToken);

                if (entity != null)
                {
                    result.Result = _mapper.Map<QRModelDto>(entity);
                }
                else
                {
                    result.Error = ResultError.NotFound;
                }

                return result;
            }
        }
    }
}