using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.QRModel
{
    public class DeleteQRModel
    {
        public class Request : IRequest<ResultDto<QRModelDto>>
        {
            public int Id { get; set; }
        }   
        
        public class Handler : IRequestHandler<Request, ResultDto<QRModelDto>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public  async Task<ResultDto<QRModelDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<QRModelDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity = new Data.Model.QRModel {Id = request.Id};
                _context.QRModel.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<QRModelDto>(entity);

                return result;
            }
        }
    }
    
}