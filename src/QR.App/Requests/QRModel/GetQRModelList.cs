using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;
namespace QR.App.Requests.QRModel
{
  public class GetQRModelList
  {
    public class Request : IRequest<ResultDto<QRModelDto[]>>
    {

    }

    public class Handler : IRequestHandler<Request, ResultDto<QRModelDto[]>>
    {
      private readonly ApplicationDbContext _context;
      private readonly IMapper _mapper;

      public Handler(ApplicationDbContext context, IMapper mapper)
      {
        _context = context ?? throw new ArgumentNullException(nameof(context));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
      }

      public async Task<ResultDto<QRModelDto[]>> Handle(Request request, CancellationToken cancellationToken)
      {
        var result = new ResultDto<QRModelDto[]>();
        var list = _context.QRModel.Include(x => x.CreatedBy).Include(y => y.UpdatedBy).AsQueryable();
        result.Result = await list
            .Select(x => _mapper.Map<QRModelDto>(x))
            .ToArrayAsync(cancellationToken);

        return result;
      }
    }
  }
}