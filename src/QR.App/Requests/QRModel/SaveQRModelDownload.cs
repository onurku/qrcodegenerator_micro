using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.QRModel
{
  public class SaveQRModelDownload
  {
    public class Request : IRequest<IActionResult>
    {
      public int Id { get; set; }
    }

    public class Handler : IRequestHandler<Request, IActionResult>
    {
      private readonly ApplicationDbContext _context;
      private readonly IMapper _mapper;

      public Handler(ApplicationDbContext context, IMapper mapper)
      {
        _context = context ?? throw new ArgumentNullException(nameof(context));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
      }

      public async Task<IActionResult> Handle(Request request, CancellationToken cancellationToken)
      {
        // Get the QR code from the database
        var qrCodeModel = await _context.QRModel.FindAsync(request.Id);

        // Convert the byte[] to a Bitmap
        Bitmap qrCodeImage;
        using (var ms = new MemoryStream(qrCodeModel.QRCodeImage))
        {
          qrCodeImage = new Bitmap(ms);
        }

        // Convert to output as PNG format
        var stream = new MemoryStream();
        qrCodeImage.Save(stream, ImageFormat.Png);
        stream.Position = 0;

        // Return response for download
        var resultDto = _mapper.Map<QRModelDto>(qrCodeModel);
        return ReturnFile(stream, "image/png", "QRCode.png");
      }
    }

    static IActionResult ReturnFile(MemoryStream stream, string contentType, string fileDownloadName)
    {
      return new FileStreamResult(stream, contentType)
      {
          FileDownloadName = fileDownloadName
      };
    }
  }
}
