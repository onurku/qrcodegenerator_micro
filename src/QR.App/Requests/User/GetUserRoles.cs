using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.User {
    public class GetUserCompanyRoles {
        public class Request : IRequest<ResultDto<CompanyRolesDto[]>> {
            public int CompanyId { get; set; }
        }

        public class Handler : IRequestHandler<Request, ResultDto<CompanyRolesDto[]>> {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler (ApplicationDbContext context, IMapper mapper) {
                _context = context ??
                    throw new ArgumentNullException (nameof (context));
                _mapper = mapper ??
                    throw new ArgumentNullException (nameof (mapper));
            }

            public async Task<ResultDto<CompanyRolesDto[]>> Handle (Request request, CancellationToken cancellationToken) {
                var result = new ResultDto<CompanyRolesDto[]> ();
                var list = _context.CompanyRoles.Where(x=> x.CompanyId == request.CompanyId).AsQueryable();

                result.Result = await list.Select (x => _mapper.Map<CompanyRolesDto> (x))
                    .ToArrayAsync (cancellationToken: cancellationToken);

                return result;
            }
        }
    }
}