using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.User
{
    public class GetUserCompany
    {
        public class Request : IRequest<ResultDto<CompanyDto>>
        {
            public string UserId { get; set; }
        }
        
        public class Handler : IRequestHandler<Request, ResultDto<CompanyDto>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }

            public Task<ResultDto<CompanyDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<CompanyDto>();
                if (string.IsNullOrEmpty(request.UserId)) throw new ArgumentNullException(nameof(request.UserId));

                var user = _context.Users
                    .Where(x => x.Id == request.UserId)
                    .FirstOrDefaultAsync(cancellationToken: cancellationToken);
                var company = _context.Company.Find(user.Result.CompanyId);

                result.Result = user.Result.CompanyId == 0 ? null : _mapper.Map<CompanyDto>(company);

                return Task.FromResult(result);
            }
        }
    }
}