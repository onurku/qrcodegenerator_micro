using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.Company
{
    public class GetCompanyList
    {
        public class Request : IRequest<ResultDto<CompanyDto[]>>
        {
            public bool? IsActive { get; set; }
        }

        public class Handler : IRequestHandler<Request, ResultDto<CompanyDto[]>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }

            public async Task<ResultDto<CompanyDto[]>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<CompanyDto[]>();

                var list = _context.Company.AsQueryable();

                if (request.IsActive.HasValue)
                {
                    list = list.Where(x => x.IsActive == request.IsActive.Value);
                }

                result.Result = await list
                    .OrderBy(x => x.Name)
                    .Select(x => _mapper.Map<CompanyDto>(x))
                    .ToArrayAsync(cancellationToken);

                return result;
            }
        }
    }
}