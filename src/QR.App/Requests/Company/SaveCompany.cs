using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests.Company
{
    public class SaveCompany
    {
        public class Request : IRequest<ResultDto<CompanyDto>>
        {
            public int Id { get; set; }
            public CompanyPostDto Post { get; set; }
        }
        
        public class Handler : IRequestHandler<Request, ResultDto<CompanyDto>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            
            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }

            public async Task<ResultDto<CompanyDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<CompanyDto>();
                if (request.Post == null) throw new ArgumentNullException(nameof(request.Post));

                var entity = _mapper.Map<Data.Model.Company>(request.Post);

                if (request.Id <= default(int))
                {
                    _context.Company.Add(entity);
                }
                else
                {
                    entity.Id = request.Id;
                    _context.Company.Update(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<CompanyDto>(entity);

                return result;
            }
        }
    }
}