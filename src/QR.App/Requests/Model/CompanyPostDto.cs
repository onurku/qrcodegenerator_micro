﻿namespace QR.App.Requests.Model {
    public class CompanyPostDto {
        public string Name { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Region { get; set; }
        public string Neighborhood { get; set; }
        public string Description { get; set; }
        public string Postcode { get; set; }
        public bool IsActive { get; set; }
        public bool Logo1Var { get; set; }
        public string Logo1Adi { get; set; }
        public int? Logo1Id { get; set; }
        public bool Logo2Var { get; set; }
        public string Logo2Adi { get; set; }
        public int? Logo2Id { get; set; }
    }
}