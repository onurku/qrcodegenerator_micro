﻿using System;

namespace QR.App.Requests.Model
{
  public class QRModelPostDto
  {
    public string Url { get; set; }
    public string ColorDark { get; set; }
    public string ColorLight { get; set; }
    public int? ImageHeight { get; set; }
    public int? ImageWidth { get; set; }
    public int? Width { get; set; }
    public string Title { get; set; }

    public bool Generated { get; set; }

    public bool LogoExist { get; set; }
    public DateTime CreateDate { get; set; }
    public DateTime UpdateDate { get; set; }

  }
}