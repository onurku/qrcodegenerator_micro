﻿namespace QR.App.Requests.Model
{
    public class CompanyRolesPostDto
    {
        public string Role { get; set; }
        public int CompanyId { get; set; }
    }
}