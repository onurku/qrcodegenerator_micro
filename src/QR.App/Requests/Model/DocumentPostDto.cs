﻿namespace QR.App.Requests.Model {
    public class DocumentPostDto {
        public string OrginalName { get; set; }
        public string Path { get; set; }
        public string Channel { get; set; }
        public int ModelId { get; set; } 
        public byte[] Files { get; set; }

    }
}