﻿namespace QR.App.Requests.Model
{
    public class DocumentDto : DocumentPostDto
    {
        public int Id { get; set; }
    }
}