﻿namespace QR.App.Requests.Model
{
    public class CompanyDto : CompanyPostDto
    {
        public int Id { get; set; }
    }
}