﻿namespace QR.App.Requests.Model
{
    public enum ResultError
    {
        None,
        NotFound,
        InUse
    }
}