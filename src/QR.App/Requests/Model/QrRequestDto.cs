﻿namespace QR.App.Requests.Model
{
  public class QrRequestDto
  {
    public string Url { get; set; }
    public string Color { get; set; }
    public bool Logo { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
  }
}