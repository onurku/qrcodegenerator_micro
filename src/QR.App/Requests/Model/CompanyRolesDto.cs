﻿namespace QR.App.Requests.Model
{
    public class CompanyRolesDto : CompanyRolesPostDto
    {
        public int Id { get; set; }
    }
}