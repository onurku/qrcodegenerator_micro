﻿namespace QR.App.Requests.Model
{
    public class RoleDto : RolePostDto
    {
        public int Id { get; set; }
    }
}