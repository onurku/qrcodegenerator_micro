﻿using System;

namespace QR.App.Requests.Model {
    public class RolePostDto {
        public string Name { get; set; }
    }
}