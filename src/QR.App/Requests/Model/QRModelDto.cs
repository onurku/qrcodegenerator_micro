﻿namespace QR.App.Requests.Model
{
  public class QRModelDto : QRModelPostDto
  {
    public int Id { get; set; }

    public string CreatedByFirstname { get; set; }
    public string UpdatedByFirstname { get; set; }
  }
}