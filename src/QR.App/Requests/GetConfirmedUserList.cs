using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QR.App.Data;
using QR.App.Requests.Model;

namespace QR.App.Requests
{
    public class GetConfirmedUserList
    {
        public class Request : IRequest<ResultDto<UserDto[]>>
        {
            
        }

        public class Handler :  IRequestHandler<Request, ResultDto<UserDto[]>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<UserDto[]>> Handle(Request request, CancellationToken cancellationToken)
            {               
                var result = new ResultDto<UserDto[]>();
                
                var list = _context.Users.Where(x => x.EmailConfirmed).AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<UserDto>(x)).ToArrayAsync(cancellationToken);

                return result;
            }
        }
    }
}