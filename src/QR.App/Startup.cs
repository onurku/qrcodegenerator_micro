using System;
using System.IO;
using System.Linq;
using AutoMapper;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using NSwag;
using NSwag.Generation.Processors.Security;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using Quartz;
using QR.App.Data;
using QR.App.Data.Model;

namespace QR.App
{
  public class Startup
  {
    private readonly IWebHostEnvironment _environment;

    public Startup(IConfiguration configuration, IWebHostEnvironment environment)
    {
      _environment = environment;
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      // var dbConnStr = Configuration.GetConnectionString ("DbConnectionString");
      // if (dbConnStr.Contains (".db")) {
      //     services.AddDbContext<ApplicationDbContext> (options =>
      //         options.UseSqlite (
      //             Configuration.GetConnectionString ("DbConnectionString")));
      // } else {
      services.AddDbContext<ApplicationDbContext>(options =>
          options.UseMySql(
              Configuration.GetConnectionString("DbConnectionString"),
              m => m.ServerVersion(new ServerVersion(new Version(10, 5), ServerType.MariaDb))));

      // }
      services.Configure<ForwardedHeadersOptions>(options =>
      {
        options.ForwardedHeaders =
            ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
      });
      services.Configure<FormOptions>(o =>
      {
        o.ValueLengthLimit = int.MaxValue;
        o.MultipartBodyLengthLimit = int.MaxValue;
        o.MemoryBufferThreshold = int.MaxValue;
      });

      services.AddIdentity<ApplicationUser, IdentityRole>(options =>
      {
        options.SignIn.RequireConfirmedAccount = true;
        options.Stores.MaxLengthForKeys = 85;
      })
          .AddRoles<IdentityRole>()
          .AddEntityFrameworkStores<ApplicationDbContext>()
          .AddDefaultTokenProviders();
      services.AddIdentityServer()
          .AddApiAuthorization<ApplicationUser, ApplicationDbContext>()
          .AddProfileService<ProfileService>();
      services.AddAuthentication()
          .AddIdentityServerJwt();

      services.AddControllersWithViews();
      services.AddRazorPages();
      // In production, the Angular files will be served from this directory
      services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });

      services.AddOpenApiDocument(configure =>
      {
        configure.Title = "QR Studio";
        configure.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
        {
          Type = OpenApiSecuritySchemeType.ApiKey,
          Name = "Authorization",
          In = OpenApiSecurityApiKeyLocation.Header,
          Description = "Type into the textbox: Bearer {your JWT token}."
        });

        configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
      });
      services.ConfigureApplicationCookie(options =>
      {
        options.LoginPath = $"/Identity/Account/Login";
        options.LogoutPath = $"/Identity/Account/Logout";
        options.AccessDeniedPath = $"/Identity/Account/AccessDenied";

      });
      services.AddControllers();
      PerformCorsSetup(services);

      services.AddAutoMapper(typeof(Startup));
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      services.AddTransient<Bootstrap>();
      services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
      services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));

      services.AddMediatR(typeof(Startup));

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.UseForwardedHeaders();
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseDatabaseErrorPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
      }

      app.Use((context, next) =>
      {
        context.Request.Scheme = "https";
        return next();
      });
      app.UseHttpsRedirection();
      app.UseStaticFiles();

      if (!Directory.Exists("StaticFiles"))
      {
        Directory.CreateDirectory("StaticFiles");
      }
      app.UseStaticFiles(new StaticFileOptions()
      {
        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"StaticFiles")),
        RequestPath = new PathString("/StaticFiles")
      });
      if (!env.IsDevelopment())
      {
        app.UseSpaStaticFiles();
      }

      app.UseRouting();

      app.UseAuthentication();

      app.UseIdentityServer();
      app.UseAuthorization();
      app.UseStatusCodePages(context =>
      {
        var response = context.HttpContext.Response;
        if (response.StatusCode == (int)System.Net.HttpStatusCode.Unauthorized ||
            response.StatusCode == (int)System.Net.HttpStatusCode.Forbidden)
          response.Redirect("/home");
        return System.Threading.Tasks.Task.CompletedTask;
      });
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute(
            name: "default",
            pattern: "{controller}/{action=Index}/{id?}");
        endpoints.MapRazorPages();
      });

      app.UseOpenApi(c => { });
      app.UseSwaggerUi3(c => { });

      app.UseSpa(spa =>
      {
        // To learn more about options for serving an Angular SPA from ASP.NET Core,
        // see https://go.microsoft.com/fwlink/?linkid=864501

        spa.Options.SourcePath = "ClientApp/dist";
        if (env.IsDevelopment())
        {
          spa.UseAngularCliServer(npmScript: "start");
        }
      });
      app.UseCookiePolicy(new CookiePolicyOptions { MinimumSameSitePolicy = SameSiteMode.Lax });
    }
    private static void PerformCorsSetup(IServiceCollection services)
    {
      // ********************
      // Setup CORS
      // ********************
      var corsBuilder = new CorsPolicyBuilder();
      corsBuilder.AllowAnyHeader();
      corsBuilder.AllowAnyMethod(); // For anyone access.
                                    //corsBuilder.WithOrigins("http://localhost:56573"); // for a specific url. Don't add a forward slash on the end!

      services.AddCors(o =>
      {
        o.AddPolicy("AllowOrigin", builder =>
        {
          builder
              .WithOrigins("http://164.92.185.61", "https://164.92.185.61",
                  "http://164.92.185.61:80", "https://164.92.185.61:443",
                  "http://localhost:5000", "https://localhost:5001")
              .AllowAnyHeader()
              .AllowAnyMethod();
        });
      });
      services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
          .AddCookie(options =>
          {
            options.Cookie.Name = "QR";
            options.Cookie.HttpOnly = false;
            options.Cookie.SameSite = SameSiteMode.Lax;
            options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            options.Cookie.IsEssential = true;
            options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
            options.LoginPath = "/Identity/Account/Login";
            options.LogoutPath = "/Identity/Account/Logout";
            options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            options.SlidingExpiration = true;
          });
    }
  }
}