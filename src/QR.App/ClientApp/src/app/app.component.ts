import { Component, OnInit, Renderer2 } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeTR from '@angular/common/locales/tr';
import { Inject, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Location } from '@angular/common';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { trigger, transition, query, style, animate, state } from '@angular/animations';
import { SharedService } from './services/SharedService';
import { AuthorizeService } from 'src/api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { UserService } from './services/QR-api';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('routerTransition', [
      transition('* <=> *', [
        query(':enter, :leave', [
          style({
            opacity: 0,
            display: 'flex',
            flex: '1',
            flexDirection: 'column'
          }),
        ], { optional: true }),
        query(':enter', [
          animate('600ms ease', style({ opacity: 1 })),
        ], { optional: true }),
        query(':leave', [
          animate('600ms ease', style({ opacity: 0 })),
        ], { optional: true })
      ]),
    ]),
    trigger('slide', [
      state('left', style({ transform: 'translateX(0)' })),
      state('right', style({ transform: 'translateX(-85%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class AppComponent implements OnInit {

  public isLogedIn: boolean;
  private _userService: UserService;
  private _sharedService: SharedService;
  public showMenu = true;
  @ViewChild(NavbarComponent, { static: false }) navbar: NavbarComponent;
  constructor(private authService: AuthorizeService,
    private userService: UserService,
    private renderer: Renderer2,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private element: ElementRef,
    public location: Location,
    private sharedService: SharedService, private cookieService: CookieService) {
    this._userService = userService;
    this._sharedService = sharedService;
  }
  title = 'QR Simulation Studio';
  ngOnInit() {

    this.authService.isAuthenticated().subscribe((data) => {
      this.isLogedIn = data;
      if (this.isLogedIn) {
        this._userService.getUserCompanybyId().subscribe(comp => {
          this._sharedService.setCompany(comp.result);
        });

      }
    });


    registerLocaleData(localeTR, 'tr');
    const navbar: HTMLElement = this.element.nativeElement.children[0].children[0];
    this.renderer.listen('window', 'scroll', (event) => {
      const number = window.scrollY;
      if (number > 150 || window.pageYOffset > 150) {
        // add logic
        navbar.classList.remove('navbar-transparent');
      } else {
        // remove logic
        navbar.classList.add('navbar-transparent');
      }
    });
    const ua = window.navigator.userAgent;
    const trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      const rv = ua.indexOf('rv:');
      const version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      if (version) {
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('ie-background');
      }
    }
  }
  removeFooter() {
    let titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if (titlee === 'signup') {
      return false;
    } else {
      return true;
    }
  }

}
