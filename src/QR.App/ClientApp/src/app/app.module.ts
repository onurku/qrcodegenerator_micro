
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { UserService, BASE_URL, QRModelService, DocumentService } from 'src/app/services/QR-api';
import { environment } from '../environments/environment';
import { NgxUiLoaderConfig, NgxUiLoaderModule, NgxUiLoaderRouterModule } from 'ngx-ui-loader';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeModule } from './home/home.module';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from 'src/api-authorization/login/login.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AlertService } from './services/alert.service';
import { QrGenerateComponent } from './qr-generate/qr-generate.component';
import { HomeComponent } from './home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QRCodeModule } from 'angularx-qrcode';
import { GrListComponent } from './gr-list/gr-list.component';
function getBaseUrl() {
  return environment.BaseUrl;
}

const ngxUiLoaderConfigT: NgxUiLoaderConfig =
{
  "bgsColor": "#ecf87f",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "ball-spin-clockwise",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#a0ea28",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "pulse",
  "gap": 24,
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(40, 40, 40, 0.8)",
  "pbColor": "red",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    QrGenerateComponent,
    GrListComponent
  ],
  imports: [
    CommonModule,
    HomeModule,
    HttpClientModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    ApiAuthorizationModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    QRCodeModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfigT),
    NgxUiLoaderRouterModule.forRoot({ showForeground: false }),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'home', component: HomeComponent, pathMatch: 'full' },
      { path: '*/Error', component: NotFoundComponent, pathMatch: 'full' },
      { path: 'Identity/Account/Login', component: LoginComponent },
      { path: 'authentication/logged-out', component: HomeComponent },
      { path: 'authentication/login', component: LoginComponent },
      { path: 'authentication/login-callback', redirectTo: 'authentication/login', pathMatch: 'full' },
      { path: 'qr/:id', component: QrGenerateComponent, canActivate: [AuthorizeGuard] },
      { path: 'qr', component: GrListComponent, canActivate: [AuthorizeGuard] },
      { path: '404', component: NotFoundComponent },
      { path: '**', redirectTo: '/404' }
    ], { relativeLinkResolution: 'legacy' })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true },
    { provide: BASE_URL, useFactory: getBaseUrl },
    UserService, CookieService, QRModelService, AlertService, DocumentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
