import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { CompanyDto } from './QR-api';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public _company = new BehaviorSubject<CompanyDto>(new CompanyDto());
  public _editFlaq = new BehaviorSubject<boolean>(false);
  constructor() {
  }
  public setCompany(com: CompanyDto) {
    // console.log("Changed Company in Service");
    this._company.next(com);
    // console.log("Value " + JSON.stringify(com));
  }

  public toggleEditFlag(): void {
    this._editFlaq.next(!this._editFlaq.value);
  }
}
