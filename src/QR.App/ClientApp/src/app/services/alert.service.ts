import { Injectable } from '@angular/core';
import * as jQuery from 'jquery';
import 'bootstrap-notify';
declare var $: any;

@Injectable()
export class AlertService {
    [x: string]: any;
    constructor() { }
    showNotification(from, align, htmlString, color) {

        const type = ['', 'info', 'success', 'warning', 'danger'];

        $.notify({
            icon: 'notifications',
            message: htmlString,
            target: '_blank'
        }, {
            element: 'body',
            type: "info",
            timer: 1000,
            showProgressbar: true,
            placement: {
                from,
                align
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            template:
                `<div data-notify="container" class="col-xs-8 col-sm-2 alert alert-${type[color]}" role="alert">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">×</button>
                    <span data-notify="icon"></span> 
                    <span data-notify="title">{1}</span> 
                    <span data-notify="message">{2}</span>
                    <div class="progress mt-1" data-notify="progressbar">
                        <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                    </div>
                    <a href="{3}" target="{4}" data-notify="url"></a>
                </div>`
        });
    }
}
