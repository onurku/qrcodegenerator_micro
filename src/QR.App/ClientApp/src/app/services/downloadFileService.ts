import { Injectable } from '@angular/core';
import { DocumentService } from './QR-api';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class DownloadFileService {
  public _documentService: DocumentService;
  constructor(private documentService: DocumentService) {
    this._documentService = documentService;
  }

  download(DocId: number, DocName: string) {
    return this._documentService.getDownloadbyId(DocId).subscribe(
      success => {
        saveAs(success, DocName);
      },
      err => {
        alert("Problem while downloading the file.");
        console.error(err);
      }
    );
  }



}

