import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CompanyDto, DocumentService, QRModelDto, QRModelService } from '../services/QR-api';
import { AlertService } from '../services/alert.service';
import { DownloadFileService } from '../services/downloadFileService';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { SharedService } from '../services/SharedService';
@Component({
  selector: 'app-qr-generate',
  templateUrl: './qr-generate.component.html',
  styleUrls: ['./qr-generate.component.css']
})
export class QrGenerateComponent implements OnInit {
  @Input() public QrId: number;
  @Output() public onUploadFinished = new EventEmitter();
  public qrdata = 'asdasdasdadsd';
  public colorDark = '#000000ff';
  public colorLight = '#ffffffff';
  public imageHeight = 0;
  public imageWidth = 0;
  public width = 300;
  public title = '';
  public QRObject: QRModelDto;
  private _qrService: QRModelService;
  private _documentService: DocumentService;
  private _sharedService: SharedService;
  public progressLogo: number;
  public messageLogo: string;
  private company: CompanyDto;
  public QRForm = this.formBuilder.group({
    URL: new FormControl('https://', Validators.required),
    imageWidth: new FormControl(100, [Validators.required, Validators.pattern("^[0-9]*$")]),
    imageHeight: new FormControl(100, Validators.required),
    colorLight: new FormControl('#f8f9fa', Validators.required),
    colorDark: new FormControl('#343a40', Validators.required),
    qRWidth: new FormControl(100, Validators.required),
    title: new FormControl('QR Code Title', Validators.required),
    logoExist: new FormControl(false)
  });
  get f() { return this.QRForm.controls; }
  public changeFlaq = false;
  public submited = false;
  constructor(private formBuilder: FormBuilder,
    private qrservice: QRModelService,
    private alertService: AlertService,
    private documentService: DocumentService, private downloadService: DownloadFileService, private http: HttpClient,
    private sharedService: SharedService) {
    this._qrService = qrservice;
    this._documentService = documentService;
    this._sharedService = sharedService;

  }

  ngOnInit(): void {
    this.QRObject = new QRModelDto();
    this._sharedService._company.subscribe(company => {
      this.company = company;
      this.QRCall();
    });
  }

  generateQRCode(): void {
    this.submited = true;
    this.changeFlaq = true;
    if (this.QRForm.valid) {
      this.QRObject.url = this.QRForm.controls['URL'].value;
      this.QRObject.imageWidth = +this.QRForm.controls['imageWidth'].value;
      this.QRObject.imageHeight = +this.QRForm.controls['imageHeight'].value;
      this.QRObject.colorLight = this.QRForm.controls['colorLight'].value;
      this.QRObject.colorDark = this.QRForm.controls['colorDark'].value;
      this.QRObject.width = +this.QRForm.controls['qRWidth'].value;
      this.QRObject.title = this.QRForm.controls['title'].value;
      this.QRObject.logoExist = this.QRForm.controls['logoExist'].value;
      this._qrService.addQRModel(this.QRObject).subscribe(res => {
        if (res.isSuccessfull) {
          this.alertService.showNotification('bottom', 'right', 'QR Code is generated ', 1);
          this.changeFlaq = false;
          this._sharedService.toggleEditFlag();
        } else {
          this.changeFlaq = false;
        }
      });
    } else {
      this.alertService.showNotification('top', 'right', 'ERROR ::: Validated Fields:' + this.findInvalidControls(), 1);
    }
  }
  findInvalidControls() {
    const invalid = [];
    const controls = this.QRForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  QRCall() {
    if (this.QrId === 0) {
      this.QRObject = new QRModelDto();
    } else {
      this._qrService.getQRModelbyId(this.QrId).subscribe(model => {
        if (model.isSuccessfull) {
          this.QRObject = model.result;
        }
      }, error => {
        console.log(error);
      });
    }
  }


  dosyaGetir(Id: number, dosyaAdi: string) {
    this.downloadService.download(Id, dosyaAdi)
  }
  public uploadLogo = (files) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    // console.log("typeUrl >>> " + typeUrl);
    this.http.post('/api/upload/', formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progressLogo = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.messageLogo = 'Logo is uploaded'
          this.onUploadFinished.emit(event.body);
          this.QRCall();
        }
      });
  }

}
