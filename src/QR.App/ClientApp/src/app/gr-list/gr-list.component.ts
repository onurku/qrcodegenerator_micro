import { Component, OnInit } from '@angular/core';
import { QRModelDto, QRModelService } from '../services/QR-api';
import { AlertService } from '../services/alert.service';
import { animate, style, transition, trigger, state } from '@angular/animations';
import { SharedService } from '../services/SharedService';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-gr-list',
  templateUrl: './gr-list.component.html',
  styleUrls: ['./gr-list.component.css'],
  animations: [
    trigger('slideUpDown', [
      transition(':enter', [
        style({ transform: 'translateY(-100%)' }),
        animate('0.5s ease-out', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('0.5s ease-in', style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class GrListComponent implements OnInit {
  private _qrService: QRModelService;
  public QRmodelId = 0;
  public editFlaq = false;
  public QRList: QRModelDto[];
  public thereismodels = false;
  constructor(private qrservice: QRModelService,
    private alertService: AlertService, private sharedService: SharedService) {
    this._qrService = qrservice;
    //Toggeller
    this.sharedService._editFlaq.subscribe(res => {
      this.editFlaq = res.valueOf();
    });
  }

  ngOnInit(): void {
    this.sharedService._editFlaq.subscribe(res => {
      this.QRListCall();
    });
  }

  toggle() {
    this.sharedService.toggleEditFlag();
  }
  QRListCall(): void {
    this._qrService.getAllQRModel().subscribe(res => {
      if (res.isSuccessfull) {
        this.QRList = res.result;
        this.thereismodels = true;
      } else {
        this.thereismodels = false;
      }
    })
  }

  editQR(qr: QRModelDto): void {
    this.QRmodelId = qr.id;
    this.editFlaq = true;
  }

  deleteQR(qr: QRModelDto): void {
    if (window.confirm('Are you sure you want to delete the QR Code ?')) {
      console.log(' this.QRList[i].id ' + qr.id);
      this._qrService.deleteQRModel(qr.id).subscribe(res => {
        if (res.isSuccessfull) {
          this.alertService.showNotification('bottom', 'right', 'QR Code has been removed...', 1);
          this.QRListCall();
        }
      })
    }
  }



  downloadPng(modId: number) {
    return this._qrService.getDownloadbyId(modId).subscribe(
      success => {
        saveAs(success, "QRCode.png");
      },
      err => {
        alert("Problem while downloading the file.");
        console.error(err);
      }
    );
  }


}
