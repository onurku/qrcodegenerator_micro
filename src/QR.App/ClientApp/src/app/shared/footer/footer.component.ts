import { Component, OnInit } from '@angular/core';
import { filter, tap } from 'rxjs/operators';
import { SharedService } from 'src/app/services/SharedService';
import { AuthorizeService } from '../../../api-authorization/authorize.service';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    public yearstr: Date = new Date();
    public loginflag = false;
    constructor( private authService: AuthorizeService, private sharedService: SharedService) { }

    ngOnInit() {
        this.isLoggedIn();
    }

    isLoggedIn() {
        this.sharedService._company.subscribe((data) => {
            if (data.id > 0) {
                // console.log('sadasd '  + JSON.stringify(data));
                this.loginflag = true;
            } else {
                this.loginflag = false;
            }
        });
        // this.loginflag = this.authService.isAuthenticated ? true : false
        // console.log("dd" + this.loginflag);
    }
}
