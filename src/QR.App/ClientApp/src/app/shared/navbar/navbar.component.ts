import { Component, OnInit, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { Router } from '@angular/router';
import { HostListener } from '@angular/core';
import { faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';
import { AlertService } from 'src/app/services/alert.service';
import { User } from 'oidc-client';
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    public anotherVar = '';

    private toggleButton: any;
    public sidebarVisible: boolean;
    public isLoading = false;
    public loginflag = false;
    public images: any;
    public profileImage = '../../assets/img/faces/BasicUser.JPG';
    public adminUser = false;
    public transparentNav = true;
    private patParam: any;
    public isExpanded = false;
    private _user: User;
    faArrowCircleDown = faArrowCircleDown;
    constructor(public location: Location,
        private element: ElementRef,
        private authService: AuthorizeService,
        private router: Router, private alertService: AlertService) {
        this.sidebarVisible = false;
        this.authService.getUser().subscribe(x => {
            this._user = x;
            if (this.authService.isAuthenticated && this._user) {
                this.adminUser = true;
            }
        });
    }
    @HostListener('window:scroll', ['$event']) onWindowScroll(e) {
        let element = document.querySelector('.navbar');
        // if (e.target['scrollingElement'].scrollTop > 150 && (this.patParam === '/' || this.patParam === '/home')) {
        //     this.transparentNav = false;
        // }
        // else if  (e.target['scrollingElement'].scrollTop > 50 && this.patParam === '/contact') {
        //     this.transparentNav = false;
        // }
        // else if  (e.target['scrollingElement'].scrollTop > 150 && this.patParam === '/about') {
        //     this.transparentNav = false;
        // } else {
        //     this.transparentNav = true;
        // }
    }
    navigetTo(st: string) {
        this.router.navigateByUrl(st);
    }
    ngOnInit() {
        this.router.events.subscribe(value => {
            this.patParam = this.router.url.toString();
        });
        this.images = {
            QR_logo_sm: '../../assets/img/logo.png'
        };
        // if (this.Account.currentUser) {
        //     this.Account.getUser(this.Account.currentUser.id).subscribe(us => {
        //         if (us) {
        //             window.setTimeout(() => {
        //                 this.profileImage = us.profileImage;
        //             }, 3000);

        //         }
        //     });
        // }

        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.authService.isAuthenticated().subscribe(fl => {
            this.loginflag = fl;
        });
        //console.log('a ' + JSON.stringify(this.loginflag));
    }
    private isLoggedIn() {
        return this.authService.isAuthenticated ? true : false;
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        // console.log(toggleButton, 'toggle');

        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    }

    isHome() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/home') {
            return true;
        } else {
            return false;
        }
    }

    isLogin() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/login') {
            return true;
        } else {
            return false;
        }
    }

    toggle() {
        this.isExpanded = !this.isExpanded;
    }

}
