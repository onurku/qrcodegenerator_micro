import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizeService, IUser } from './authorize.service';
import { ApplicationPaths, QueryParameterNames } from './api-authorization.constants';
import { User } from 'oidc-client';


@Injectable({
  providedIn: 'root'
})
export class AuthorizeGuard implements CanActivate {
  private _user: User;
  constructor(private authorize: AuthorizeService, private router: Router) {
    this.authorize.getUser().subscribe(x => {
      this._user = x;
    });
  }

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.authorize.getUser().subscribe(x => {
      // console.log(JSON.stringify(x));
    });
    //if (this.authorize.isAuthenticated().pipe(tap(isAuthenticated => {this.handleAuthorization(isAuthenticated, state) }))) {

    if (this.authorize.isAuthenticated && this._user) {
      return true;
    } else {
      this.router.navigate(['authentication/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }

  private handleAuthorization(isAuthenticated: boolean, state: RouterStateSnapshot) {
    if (!isAuthenticated) {
      this.router.navigate(ApplicationPaths.LoginPathComponents, {
        queryParams: {
          [QueryParameterNames.ReturnUrl]: state.url
        }
      });
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthorizeAdminGuard implements CanActivate {
  private _user: User;

  constructor(private authorize: AuthorizeService, private router: Router) {
    this.authorize.getUser().subscribe(x => {
      this._user = x;
    });
  }

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authorize.isAuthenticated && this._user) {
      return true;
    } else {
      this.router.navigate(['authentication/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthorizeGroupGuard implements CanActivate {
  private _user: User;
  private _groupId: number;

  constructor(private authorize: AuthorizeService, private router: Router) {
    this.authorize.getUser().subscribe(x => {
      this._user = x;
    });


  }

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authorize.isAuthenticated && this._user) {
      return true;
    } else {
      this.router.navigate(['authentication/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}

