namespace QR.App.Core.EmailTemplate.Model
{
    public class ConfirmEmailModel
    {
        public string Fullname { get; set; }
        public string CallbackUrl { get; set; }
    }
}