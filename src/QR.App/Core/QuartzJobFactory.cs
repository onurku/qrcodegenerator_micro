using System;
using Quartz;
using Quartz.Spi;

namespace QR.App.Core
{
  public class QuartzJobFactory : IJobFactory
  {
    private readonly System.IServiceProvider _serviceProvider;
    public QuartzJobFactory(IServiceProvider serviceProvider)
    {
      _serviceProvider = serviceProvider;
    }
    public IJob NewJob(TriggerFiredBundle triggerFiredBundle,
    IScheduler scheduler)
    {
      var jobDetail = triggerFiredBundle.JobDetail;
      return (IJob)_serviceProvider.GetService(jobDetail.JobType);
    }
    public void ReturnJob(IJob job) { }
  }
}