using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QR.App.Requests.Model;
using QR.App.Requests.User;

namespace QR.App.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserController(IMediator mediator, IHttpContextAccessor httpContextAccessor)
        {
            _mediator = mediator;
            _httpContextAccessor = httpContextAccessor;
        }


                /// <summary>
        /// Kullanicinin erisebildigi gruplar
        /// </summary>
        /// <returns>Group</returns>
        [HttpGet("/userRoles/{Compid}")]
        [ProducesResponseType(typeof(ResultDto<CompanyRolesDto[]>), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ResultDto<CompanyRolesDto[]>> GetRoles(int Compid)
        {
            try
            {
                return await _mediator.Send(new GetUserCompanyRoles.Request {CompanyId = Compid});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

                /// <summary>
        /// Kullanicinin erisebildigi gruplar
        /// </summary>
        /// <returns>Group</returns>
        [HttpGet("/Usercompany")]
        [ProducesResponseType(typeof(ResultDto<CompanyDto>), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ResultDto<CompanyDto>> GetUserCompany()
        {
            try
            {
                var userId = _httpContextAccessor.HttpContext.User.FindFirst("sub").Value;
                return await _mediator.Send(new GetUserCompany.Request {UserId = userId});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}