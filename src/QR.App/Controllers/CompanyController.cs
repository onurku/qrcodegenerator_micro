﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QR.App.Requests.Company;
using QR.App.Requests.Model;

namespace QR.App.Controllers {
    [Authorize]
    [Route ("api/[controller]")]
    public class CompanyController : Controller {
        private readonly IMediator _mediator;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CompanyController (IMediator mediator, IHttpContextAccessor httpContextAccessor) {
            _mediator = mediator;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Sistemdeki tüm Companylar
        /// </summary>
        /// <returns>IList</returns>
        [HttpGet]
        [ProducesResponseType (typeof (ResultDto<CompanyDto[]>), 200)]
        public async Task<ResultDto<CompanyDto[]>> GetAll () {
            try {
                return await _mediator.Send (new GetCompanyList.Request ());
            } catch (Exception e) {
                Console.WriteLine (e);
                throw;
            }
        }

        /// <summary>
        /// Id ile Company
        /// </summary>
        /// <returns>Company</returns>
        [HttpGet ("{id}")]
        [ProducesResponseType (typeof (ResultDto<CompanyDto>), 200)]
        [ProducesResponseType (typeof (void), 404)]
        public async Task<ResultDto<CompanyDto>> GetById (int id) {
            try {
                return await _mediator.Send (new GetCompanyById.Request { Id = id });
            } catch (Exception e) {
                Console.WriteLine (e);
                throw;
            }
        }

        /// <summary>
        /// Company ekle
        /// </summary>
        /// <returns>Company</returns>
        [HttpPost]
        [ProducesResponseType (typeof (ResultDto<CompanyDto>), 200)]
        public async Task<ResultDto<CompanyDto>> Create ([FromBody] CompanyPostDto post) {
            try {
                return await _mediator.Send (new SaveCompany.Request { Post = post });
            } catch (Exception e) {
                Console.WriteLine (e);
                throw;
            }
        }

        /// <summary>
        /// Company guncelle
        /// </summary>
        /// <returns>Company</returns>
        [HttpPut ("{id}")]
        [ProducesResponseType (typeof (ResultDto<CompanyDto>), 200)]
        public async Task<ResultDto<CompanyDto>> Update (int id, [FromBody] CompanyPostDto post) {
            try {
                return await _mediator.Send (new SaveCompany.Request { Id = id, Post = post });
            } catch (Exception e) {
                Console.WriteLine (e);
                throw;
            }
        }

        /// <summary>
        /// Company sil
        /// </summary>
        /// <returns>Company</returns>
        [HttpDelete ("{id}")]
        [ProducesResponseType (typeof (ResultDto<CompanyDto>), 200)]
        public async Task<ResultDto<CompanyDto>> Delete (int id) {
            try {
                return await _mediator.Send (new DeleteCompany.Request { Id = id });
            } catch (Exception e) {
                Console.WriteLine (e);
                throw;
            }
        }


    }
}