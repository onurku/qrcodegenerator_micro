﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QR.App.Data.Model;
using QR.App.Requests.QRModel;
using QR.App.Requests.Model;
using Microsoft.AspNetCore.Identity;

namespace QR.App.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  public class QRModelController : Controller
  {
    private readonly IMediator _mediator;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly UserManager<ApplicationUser> _userManager;
    public QRModelController(IMediator mediator, IHttpContextAccessor httpContextAccessor, UserManager<ApplicationUser> userManager)
    {
      _mediator = mediator;
      _httpContextAccessor = httpContextAccessor;
      _userManager = userManager;
    }

    /// <summary>
    /// Sistemdeki tüm QRModellar
    /// </summary>
    /// <returns>IList</returns>
    [HttpGet]
    [ProducesResponseType(typeof(ResultDto<QRModelDto[]>), 200)]
    public async Task<ResultDto<QRModelDto[]>> GetAll()
    {
      try
      {
        return await _mediator.Send(new GetQRModelList.Request());
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }

    /// <summary>
    /// Id ile QRModel
    /// </summary>
    /// <returns>QRModel</returns>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(ResultDto<QRModelDto>), 200)]
    [ProducesResponseType(typeof(void), 404)]
    public async Task<ResultDto<QRModelDto>> GetById(int id)
    {
      try
      {
        return await _mediator.Send(new GetQRModelById.Request { Id = id });
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }

    /// <summary>
    /// QRModel ekle
    /// </summary>
    /// <returns>QRModel</returns>
    [HttpPost]
    [ProducesResponseType(typeof(ResultDto<QRModelDto>), 200)]
    public async Task<ResultDto<QRModelDto>> Create([FromBody] QRModelPostDto post)
    {
      try
      {

        var user = await _userManager.GetUserAsync(HttpContext.User);
        return await _mediator.Send(new SaveQRModel.Request { UserId = user.Id, Post = post });
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }

    /// <summary>
    /// QRModel guncelle
    /// </summary>
    /// <returns>QRModel</returns>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(ResultDto<QRModelDto>), 200)]
    public async Task<ResultDto<QRModelDto>> Update(int id, [FromBody] QRModelPostDto post)
    {
      try
      {
        var user = await _userManager.GetUserAsync(HttpContext.User);
        return await _mediator.Send(new SaveQRModel.Request { Id = id, UserId = user.Id, Post = post });
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }
  
    [HttpGet("download/{id}")]
    public async Task<IActionResult> DownloadQRasPng(int id)
    {
      try
      {
        return await _mediator.Send(new SaveQRModelDownload.Request { Id = id });
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }




    /// <summary>
    /// QRModel sil
    /// </summary>
    /// <returns>QRModel</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(typeof(ResultDto<QRModelDto>), 200)]
    public async Task<ResultDto<QRModelDto>> Delete(int id)
    {
      try
      {
        return await _mediator.Send(new DeleteQRModel.Request { Id = id });
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }


  }
}