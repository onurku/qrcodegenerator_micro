﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QR.App.Requests.Model;
using QRCoder;
using System;
using System.Drawing;
using System.IO;

namespace QR.App.Controllers
{
  [Authorize]
  [Route("api/bo/qr")]
  public class QrController : Controller
  {
    [HttpPost("make-url-code")]
    public IActionResult MakeUrlCode([FromBody] QrRequestDto request)
    {
      // validate request data
      if (string.IsNullOrEmpty(request.Url))
      {
        return BadRequest("Url is required.");
      }

      int height = request.Height > 0 ? request.Height : 200; // default height is 200
      int width = request.Width > 0 ? request.Width : 200; // default width is 200

      // create QR code bitmap image
      Bitmap qrCodeImage = GenerateQRCodeV(request.Url, request.Color, request.Logo, width, height);

      // create a memory stream and save the image to it
      MemoryStream ms = new MemoryStream();
      qrCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
      ms.Seek(0, SeekOrigin.Begin);

      // return image as response
      return File(ms, "image/png");
    }

    public static Bitmap GenerateQRCodeV(string url, string darkColor = "#000", bool logoExist = false, int width = 200, int height = 200)
    {
      // QR Code Generate
      QRCodeGenerator qrGenerator = new QRCodeGenerator();
      QRCodeData qrCodeData = qrGenerator.CreateQrCode(url, QRCodeGenerator.ECCLevel.Q);
      QRCode qrCode = new QRCode(qrCodeData);

      // QR kodunu bir bitmap olarak oluştur

      Bitmap qrCodeImage = qrCode.GetGraphic(20, darkColor, "#fff");

      // Logo place
      if (logoExist)
      {
        string fullPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Angular.png");
        Bitmap logo = new Bitmap(fullPath);

        // Logo boyutunu değiştir
        int logoWidth = (int)(qrCodeImage.Width * 0.2); // %20 genişlet
        int logoHeight = (int)(qrCodeImage.Height * 0.2); // %20 yükseklet
        Image resizedLogo = logo.GetThumbnailImage(logoWidth, logoHeight, null, IntPtr.Zero);

        // Logoyu resmin merkezine yerleştirin
        int centerX = (qrCodeImage.Width - resizedLogo.Width) / 2;
        int centerY = (qrCodeImage.Height - resizedLogo.Height) / 2;
        Rectangle logoRect = new Rectangle(centerX, centerY, resizedLogo.Width, resizedLogo.Height);

        // Logoyu resme çizin
        using (Graphics graphics = Graphics.FromImage(qrCodeImage))
        {
          graphics.DrawImage(resizedLogo, logoRect);
        }
      }

      // Resmi yeniden boyutlandır
      Bitmap resizedImage = new Bitmap(qrCodeImage, width, height);
      return resizedImage;
    }
  }
}
