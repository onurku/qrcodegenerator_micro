using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QR.App.Data.Model;

namespace QR.App.Data {
  public class Bootstrap {
    private readonly ApplicationDbContext _dbContext;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public Bootstrap (ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) {
      _dbContext = dbContext;
      _userManager = userManager;
      _roleManager = roleManager;
    }

    public async Task SeedAsync () {
      await InitializeDatabase ();
      await SeedRoles ();
      await SeedDomain ();
      await SeedUsers ();
    }

    private async Task SeedDomain () {
      var Company = await CreateCompany ();
    }

    private async Task<Company> CreateCompany () {
      var CompanyS = new Company {
        Id = 1,
        Name = "QRCompany",
        Province = " Türkiye",
        District = "ISTANBUL",
        Region = "ISTANBUL",
        Description = "",
        Neighborhood = "",
        Postcode = "0000",
        IsActive = true
      };
      await _dbContext.Company.AddAsync (CompanyS);
      return CompanyS;
    }

    private async Task SeedUsers () {
      await AddUser ("admin@QR.com.tr", Roles.Administrator);
      await AddUser ("user@QR.com.tr");
      await AddUser ("supervisor@QR.com.tr", Roles.Supervisor);
    }

    private async Task AddUser (string email, string role = Roles.Default) {
      if (await _userManager.FindByEmailAsync (email) == null) {

      var user = new ApplicationUser {
      UserName = email,
      Email = email,
      SecurityStamp = "secure",
      EmailConfirmed = true,
      Firstname = email,
      CompanyId = 1,
        };
        await _userManager.CreateAsync (user, "U@1qa@1qa");
        await _userManager.AddToRoleAsync (user, role);
      }
    }

    private async Task SeedRoles () {
      if (!await _roleManager.RoleExistsAsync (Roles.Default))
        await _roleManager.CreateAsync (new IdentityRole (Roles.Default));

      var adminRole = new IdentityRole (Roles.Administrator);
      if (!await _roleManager.RoleExistsAsync (Roles.Administrator)) {
        await _roleManager.CreateAsync (adminRole);

        adminRole = await _roleManager.FindByNameAsync (adminRole.Name);

        if (!await _roleManager.RoleExistsAsync (Roles.Accountant)) {
          await _roleManager.CreateAsync (new IdentityRole (Roles.Accountant));
        }
        if (!await _roleManager.RoleExistsAsync (Roles.Supervisor)) {
          await _roleManager.CreateAsync (new IdentityRole (Roles.Supervisor));
        }
      }
    }

    private async Task InitializeDatabase () {
      if (_dbContext.Database.ProviderName.Contains ("MySql")) {
        await _dbContext.Database.MigrateAsync ();
      } else {
        await _dbContext.Database.EnsureCreatedAsync ();
      }

    }
  }

  internal static class Roles {
    public const string Default = "user";
    public const string Administrator = "administrator";
    public const string Accountant = "accountant";
    public const string Supervisor = "supervisor";
  }
}