﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using QR.App.Data.Model;

namespace QR.App.Data.Model {
  [Table ("companyroles")]
  public class CompanyRoles {
    [Key]
    public int Id { get; set; }

    [Required]
    public string Role { get; set; }
    public int CompanyId { get; set; }
    public Company Company { get; set; }

  }
}