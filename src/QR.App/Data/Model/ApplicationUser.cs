using Microsoft.AspNetCore.Identity;

namespace QR.App.Data.Model {
  public class ApplicationUser : IdentityUser {
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public int? CompanyId { get; set; }
    public Company Company { get; set; }
  }
}