﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QR.App.Data.Model {
  [Table ("company")]
  public class Company {
    [Key]
    public int Id { get; set; }

    [Required]
    public string Name { get; set; }
    public string Province { get; set; }
    public string District { get; set; }
    public string Region { get; set; }
    public string Description { get; set; }
    public string Neighborhood { get; set; }

    [Required]
    public string Postcode { get; set; }
    public bool IsActive { get; set; }

  }
}