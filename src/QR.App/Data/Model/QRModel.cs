﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QR.App.Data.Model
{
  [Table("qrmodel")]
  public class QRModel
  {
    [Key]
    public int Id { get; set; }

    [Required]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime CreateDate { get; set; }
    public string CreatedById { get; set; }
    public ApplicationUser CreatedBy { get; set; }

    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime UpdateDate { get; set; }
    public string UpdatedById { get; set; }
    public ApplicationUser UpdatedBy { get; set; }
    public string Url { get; set; }
    public string ColorDark { get; set; }
    public string ColorLight { get; set; }
    public int ImageHeight { get; set; }
    public int ImageWidth { get; set; }
    public int Width { get; set; }
    public string Title { get; set; }
    public byte[] QRCodeImage { get; set; } // QR Code Png File

    public bool LogoExist { get; set; }

  }
}

