using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using MediatR;
using Microsoft.AspNetCore.Identity;
using QR.App.Data.Model;

namespace QR.App.Data
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMediator _mediator;

        public ProfileService(UserManager<ApplicationUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            var roles = await _userManager.GetRolesAsync(user);
            IList<Claim> roleClaims = roles.Select(role => new Claim(JwtClaimTypes.Role, role)).ToList();

        

            //add user claims

            roleClaims.Add(new Claim(JwtClaimTypes.Name, user.UserName));
            roleClaims.Add(new Claim(JwtClaimTypes.GivenName, $"{user.Firstname} {user.Lastname}"));
            context.IssuedClaims.AddRange(roleClaims);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.CompletedTask;
        }
    }
}