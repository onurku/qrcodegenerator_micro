using AutoMapper;
using QR.App.Data.Model;
using QR.App.Requests.Model;

namespace QR.App.Data
{
  public class AutoMapperProfile : Profile
  {
    public AutoMapperProfile()
    {

      CreateMap<QRModel, QRModelDto>();
      CreateMap<QRModelPostDto, QRModel>();
      CreateMap<QRModelDto, QRModel>();
      CreateMap<Company, CompanyDto>();
      CreateMap<CompanyPostDto, Company>();
      CreateMap<CompanyRoles, CompanyRolesDto>();
      CreateMap<CompanyRolesPostDto, CompanyRolesDto>();
    }
  }
}