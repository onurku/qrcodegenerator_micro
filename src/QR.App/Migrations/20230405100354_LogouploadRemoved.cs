﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QR.App.Migrations
{
    public partial class LogouploadRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "document");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "document",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Channel = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    FileS = table.Column<byte[]>(type: "longblob", nullable: true),
                    ModelId = table.Column<int>(type: "int", nullable: false),
                    OrginalName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    Path = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_document", x => x.Id);
                });
        }
    }
}
