﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QR.App.Migrations
{
    public partial class QRModelDocumenttypeRemove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "document");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "document",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
