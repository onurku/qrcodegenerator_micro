﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QR.App.Migrations
{
    public partial class QRNugetAndFieldsInTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Generated",
                table: "qrmodel",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "LogoImage",
                table: "qrmodel",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "QRCodeImage",
                table: "qrmodel",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "companyroles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Role = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_companyroles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_companyroles_company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_companyroles_CompanyId",
                table: "companyroles",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "companyroles");

            migrationBuilder.DropColumn(
                name: "Generated",
                table: "qrmodel");

            migrationBuilder.DropColumn(
                name: "LogoImage",
                table: "qrmodel");

            migrationBuilder.DropColumn(
                name: "QRCodeImage",
                table: "qrmodel");
        }
    }
}
